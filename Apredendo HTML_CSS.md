# Decolando no HTML/CSS
Iremos começar a trabalhar agora sobre o HTML e o CSS, em que ambos são linguagens voltadas para o desenvolvimento de websites. 


## O que é o HTML?
Html é uma linguagem de ~~programação~~, ou melhor dizendo, de marcação. Muitas pessoas comentem esse equívoco, a expressão ~~"Vou programar em HTML"~~ ou ~~"Sei programar em HTML"~~ é um engano, pois se trata de uma linguagem de marcação e é por esse motivo que ela é exclusivamente usada para desenvolver websites. Durante o desenvolvimento em HTML você indica, por meio do código, onde ficará o conteúdo do site, como ele ficará expresso na aba aberta do Google Chrome ou de outro navegador.

Até outras linguagens que trabalham com websites, utilizam o HTML, o CSS é uma forma de estilizar o HTML, o Javascript torna os recursos do HTML mais interativos e dinâmicos, o PHP trabalha gerando HTML de forma dinâmica, e assim vai!!

## O que iremos precisar?

Inicialmente é bem legal usar algum editor de texto, tipo ~~bloco de notas~~. Na GTi, usamos o **Visual Studio Code**, que já foi apresentado em tutoriais anteriores, ele é PERFEITO, pois auxilia em muitas coisas, principalmente em termos de produtividade. Se você, por algum motivo da galáxia, ainda não baixou o  VS code, baixe [aqui](https://code.visualstudio.com/)!

Além do editor de texto, como nosso objetivo é  desenvolver sites, o ideal é utilizar um navegador para visualizar o seu site. Afinal, é como está sendo o resultado final, é a tradução do seu código. No curso, usaremos o **Google Chrome**, já que ele é o mais comum e auxilia com algumas ferramentas nele disponíveis.

## Vamos começar!!

Primeiramente, crie uma pasta no seu computador. Em seguida, vá no VS code e clique na opção **"Abrir pasta"** e selecione a pasta criada.

![enter image description here](https://i.imgur.com/mAL0iaV.png)

Após feito isso, clique no ícone de criar novo arquivo, dê um nome a ele e ao final digite *.html* para o computador reconhecer o arquivo como HTML. Dessa forma, ao abrir o arquivo, ele será automaticamente aberto no navegador em forma de site.

## Bora ~~programar~~, ops, marcar
Para começarmos a compreender sobre HTML, é que usamos muito **TAGS**. Elas auxiliam na organização e a guiarem o site!! As tags ficam representadas entre o símbolo de maior que e menor que *(< >)*. As tags marcam o começo e o fim do conjunto de códigos. Uma tag muito utilizada é a própria *html*, se estruturando da seguinte forma: 
```html
    <html> <!--começo do bloco de códigos-->
        Códigos ficam aqui
    </html> <!--fim do bloco de códigos-->
```
 Acima temos um exemplo clássico de como toda TAG se estrutura, tendo um início, um meio (onde ficam todos os códigos) e um fim. A TAG HTML ela diz o seguinte: *"Navegador, é aqui que começa o meu site e é aqui que termina"* Os códigos entre essa tag irá englobar todo o site, seja uma parte mais backoffice (ou seja, não será exposta de maneira visual no site) ou o que será mostrado para quem acessar o site. 

Podemos utilizar atributos à uma tag, isso será muito usado, principalmente quando começarmos a trabalhar com CSS e Javascript. O atributo serve para fornecer determinadas características relacionadas com todo o bloco de códigos existente dentro da tag. Segue um exemplo abaixo:
```html
    <tag atributo="nome"> <!--essa representação é padrão-->
	    Códigos aqui
	</tag>
```
No começo do código, é essencial colocar a seguinte tag:
```html
    <!DOCTYPE html>
```
A doctype é uma tag especial, indicando como o navegador irá ler o arquivo. Existem diferentes tipos e pode variar de documento para documento, mas, no nosso caso, usaremos a doctype.

Dentro da tag html usamos também outras duas tags fundamentais:
```html
    <html>
		<head> <!--parte mais backoffice do site, configurações gerais-->
		    Códigos aqui
		</head>
		
		<body> <!--Conteúdo do site-->
		    Códigos aqui
		</body>
	</html>
```
Tendo essa estrutura, iremos trabalhar algumas configurações gerais e básicas do site. 

Primeiramente, a tipografia. A língua brasileira utiliza acentos que normalmente não são reconhecidas em outras linguagens, por isso é fundamental informar ao navegador que utilizaremos acentuações no nosso código. Para isso, usaremos o seguinte código que será inserida dentro da tag **head**.


```html
    <meta charset="utf-8"></meta>
```


   Já vou dar um bizu top das galáxias para vocês! Como podem ver, essa tag **meta** que possui infinitos usos, não possuirá nenhum bloco de comandos dentro dela, já que o único intuito é configurar o **charset** determinando o uso do nosso alfabeto, com os símbolos, acentos, etc. Por questões de simplificação, podemos fechar a tag da seguinte forma:
   
   
```html
    <meta charset="utf-8"/>
```


Esse código acima é igual ao anterior, o que muda é sua forma de representação, sendo mais prática e objetiva.

Outra tag interessante que costumamos colocar dentro do head, é a tag **title**, ela define o título do site que será exposto na aba do navegador (antes dessa configuração, fica o nome do arquivo que você inseriu quando criou com o *.html*, o que é bem ~~amador~~.


```html
    <title> Meu site top </title>
```

![enter image description here](https://i.imgur.com/4albHi7.png)

## Um pouco de formatação de texto
Aqui vamos focar no body, que é mais voltado para apresentação de conteúdo.
Primeiramente, ao digitar qualquer coisa dentro da tag body, sendo texto, ou seja, não sendo outra tag, atributo ou coisa do gênero, o navegador irá compreender como sendo um texto a ser exposto, um conteúdo em si.

```html
    <body>
	    Meu primeiro parágrafo!
	</body>
```


Contundo, se você já quiser brincar com vários parágrafos dando enter para separar eles como mostrado abaixo:


```html
     <body>
        Meu primeiro parágrafo!
        Meu segundo parágrafo!
        Meu terceiro parágrafo!
    </body>
```

Ao abrir o arquivo no navegador, você observará que ele não lhe obedeceu...

>     Meu primeiro parágrafo!Meu segundo parágrafo!Meu terceiro parágrafo!

Por que isso aconteceu? Simples, você não informou para o navegador a quebra de linha desejada, para isso usamos uma tag específica para essa finalidade, que é a  `<br/>`. Dessa forma, para o que desejo que aconteça, devo deixar o código da seguinte forma:


```html
     <body>
        Meu primeiro parágrafo!<br/>
        Meu segundo parágrafo!<br/>
        Meu terceiro parágrafo!
    </body>
```

AGORA SIM, o navegador irá entender a sua intenção!
E já que estamos falando de parágrafo, existe uma tag para isso, que é a `<p> </p>`. Escrevendo entre ela, o navegador irá dar um espaçamento superior do conteúdo acima e do abaixo, dando também uma quebra de linha automática.

   
```html
     <body>
	    <p>Olá, este é meu primeiro parágrafo!</p>
	    <p>Olá, este é meu segundo parágrafo!</p>
	    <p>Olá, este é meu terceiro parágrafo!</p>
    </body>
```

![enter image description here](https://i.imgur.com/R5ruIsu.png)

Existe também tags para definirem títulos variando da **h1** até a **h6**.

```html
     <body>
        <h1>Título H1</h1> 
        <h2>Título H2</h2> 
        <h3>Título H3</h3>
        <h4>Título H4</h4>
        <h5>Título H5</h5>
        <h6>Título H6</h6>
    </body>
```

Ao abrir o navegador, você verá claramente a diferença visual entre os títulos:

![enter image description here](https://i.imgur.com/rMewEbX.png)

Existe também tags para marcar textos que ficarão em negrito e itálico. No caso, temos a tag `<strong>Texto em negrito</strong>` e a `<em>Vai ficar em itálico</em>`.

Existem muitas outras formas de formatação, principalmente com o uso do CSS que iremos apresentar a seguir!!

# Bem-vindo ao CSS
Como já foi mencionado anteriormente, o CSS trabalhar com a estilização do HTML. É o CSS que nos ajuda a deixar o site mais bonito e com a nossa cara, ~~ou melhor, com a cara do cliente kk~~. Se você curte mecher com efeitos visuais, como eu, com certeza adorará trabalhar com CSS,~~ou não~~~.

Existem duas formas de trabalhar com o CSS, a primeira é junto com o  HTML do seu site, ou seja, trabalhar com atributos nas tags e usar todo o código CSS dentro do mesmo arquivo que o HTML. Contudo, essa prática ajuda a deixar o código mais bagunçado. Por isso, iremos para a segunda forma, que é trabalhar com o CSS em um arquivo separado do HTML, mas criando um link entre eles.

Para isso, primeiramente vamos criar um outro arquivo pelo VS Code, escolha um nome de sua preferência e no final coloque a formatação *.css* para o computador compreender que este é um arquivo para ser lido como CSS.

Feito isso, agora vamos conectar esse arquivo css criado com o nosso HTML. Em head, usaremos a seguinte tag:
```html
    <link rel="stylesheet" type="text/css" href="nome_do_arquivo.css"/>
```
O *rel* diz o que se refere ao link que estamos fazendo, que nesse caso é o estilo visual do html, o *type* o tipo de link, nesse caso com um arquivo de texto no formato css e o *href* informa a localidade, colocando na mesma pasta que o arquivo html não é necessário informar o directório, apenas o nome do arquivo.

Desse modo, já temos nosso HTML linkado com o nosso CSS. Podemos começar a brincar com o estilo da nossa página.

Primeiramente vamos compreender como se estrutura o css. Assim como no HTML, trabalhamos com as tags e atributos. No HTML criamos o body, que é o corpo do nosso site! Podemos personalizar o corpo do nosso site trabalhando com a tag **body** no css, da seguinte forma:
		
```css
    body{ 
		Configurações de estilo aqui
		}
```
Aqui estamos dizendo: "Olha, o meu body terá as seguintes configurações estéticas". Isso pode ser trabalhado com qualquer outra tag, como a`<p>`, `<h1>`, `<h2>` e assim por diante!

Uma coisa interessante é que podemos personalizar mais ainda trabalhando com atributos. Assim,conseguimos personalizar de forma mais específica determinadas áreas do nosso site. Esses atributos se referem a classes e ids. Para  ids representamos da seguinte forma:
```css
    #nome_do_meu_id {


	}
```
Para classes representamos da seguinte forma:
```css
    .nome_da_minha_classe {


	}
```

No HTML, chamamos a classe e o id dessa forma:
```html
    <tag id="nome_do_meu_id"> </tag>
	<tag class="nome_da_minha_classe"></tag>
	<tag id="nome_do_meu_id" class="nome_da_minha_classe"></tag>
```
Observe que podemos separar os dois como combiná-los, isso permite que eu personalize mais ainda determinadas áreas. Imagine a situação em que você tem dois parágrafos, você deseja que ambos possuam o mesmo tamanho de fonte, mas que um esteja na cor azul e outro na cor vermelha. Basta fazer uma classe que represente o tamanho desejado aplicando em uma tag que engloba todos os dois parágrafos e, em seguida, aplicar a id referente a cada cor na tag que abrange cada um dos parágrafos. Bem top neh?

Normalmente, usamos a tag `<div>` para trabalhar com essa divisão de áreas e assim poder usar ids e classes distintas. ~~Acredite, seu arquivos html terão muitos divs kk~~

Agora que já vimos, na teoria, o funcionamento do CSS e como trabalhar ele com o HTML, vamos estudar alguns exemplos de configurações estéticas possíveis de fazer!

## Um pouco de cores

Dar cor ao site, com certeza você irá mexer muito com isso!!! Inicialmente é importante compreender como funciona as cores na Web.

> As cores na Web são definidas pela mistura de 256 tonalidades de vermelho, de verde e de azul em diferentes proporções. Humanos tem dez dedos nas mãos e contam de dez em dez. Computadores foram projetados para contar de dezesseis em dezesseis, não que eles tenham dedos, mas uma vez ultrapassado o 9 (nove) não existe um numeral simples para representar 10, 11, 12, 13, 14, 15 assim estes numerais foram substituidos por letras a, b, c, d, e , f. Desta forma, em um sistema de contagem 'hexadecimal' (base dezesseis) o 10 é representado pelo 'a' e o 15 pelo 'f'. Ao ultrapassar o 15, adiciona-se mais um dígito e assim '10' representa o 16. Usando-se este sistema de contagem, qualquer número entre 0 (zero) e 255 pode ser representado por dois números ou letras - 255 é ff. Então, #ffffff será o branco e #000000 o preto.

Vamos para uma aplicação:

   
```css
    .body{
	background-color: #e8eae8;

	}
```
A configuração feita acima está informando que todo o conteúdo exposto no body terá como cor de plano de fundo o seguinte código de cor, que é composta de vermelho e8(232), verde ea(234), azul e8(232). O símbolo # ('tralha') indica que estamos usando numeração hexadecimal e não números decimais ordinários.

Algumas vezes você encontrará a sintaxe com apenas três dígitos, por exemplo, #2a0. Isto é uma abreviatura para #22aa00. Quando a cor é representada por uma numeração composta por três pares de dígitos que se repetem dois a dois, você poderá omitir o segundo dígito que o navegador entenderá a abreviatura.

Usando esta abreviatura com três dígitos você poderá obter 4096 cores diferentes, e com seis dígitos mais de seis milhões de cores. Com uso de um dígito por cor, vermelha, verde e azul, cada uma tendo 16 níveis de brilho é possível combinações para obter todas as demais.

Se você estiver digitando seu código, esta técnica de usar abreviadamente três dígitos é bem mais simples e eficiente.

Você também pode declarar a cor desejada por meio do nome da cor (blue, red, green, etc) ou pelo seu código RGB, que indica a porcentagem de vermelho, verde e azul que sua cor será composta.

É importante compreender esse conceito básico de trabalhar com cores, pois usaremos muito!

Mas existem quantas configurações de estilos? Existem muitas!! Por questões didáticas, iremos trabalhar com alguns exemplos para facilitar a compreenssão! 

