# Desvendando o Git
Aqui você vai aprender tudo o que precisa sobre como o *Git* é usado na **GTi**
~~Muitas vezes durante o processo você vai acabar digitando gti ao invés de git, real, acontece kkkk~~

## O que é o Git?
**Git é um sistema pra controlar versões de arquivos.** ~~Tá, mas o que é isso?~~ 
Basicamente, a gente vai desenvolver um projeto junto com alguém, **ele tenta manter tudo organizado pra ninguém perder aquilo que fez**. É tipo o que acontece no *Google Docs*, a diferença aqui é que a gente não faz isso em *tempo real*.  Além disso tudo, a gente ainda pode ver o **histórico de alterações** e **restaurar o arquivo para o que havia antes.** 

Agora imagina o caos que seria você fazer um projeto junto com alguém e não usar um sistema de versão? Você e outra pessoa editam o mesmo arquivo e na hora de juntar ficar comparando de um por um o que foi mudado? Outro, imagina que você tá desenvolvendo um site e está tudo funcionando corretamente, aí você resolve adicionar uma nova funcionalidade e ela dá errado e você não lembra exatamente tudo o que mudou?

**O GIT É UM SALVADOR DE VIDAS!** 

## Instalando o Git
Agora que a gente já entendeu o conceito, ~~mas vc ainda não faz ideia de como usar isso~~ vamos instalar o git.

- **Windows** 
	- Acesse esse [Link](https://git-scm.com/downloads), baixe a versão para windows ~~obviamente~~
	- Execute o arquivo 
	- Você pode simplesmente dar *next* em tudo que vai instalar com as configurações padrão
	- Finalizado, abra o CMD (só buscar por cmd na barra de busca)
	- Agora vamos configurar nossas credenciais, ou seja, aquilo que vai dizer que a gente foi que fez tal alteração
	- Digite `git config --global user.name 'seunome'`
	- Digite `git config --global user.email 'seuemail@email.com'`
	- Feito isso, está completa a instalação. 
- **Linux** (Ubuntu, Mint, Debian, Deepin, etc ... )
	- Abra o terminal (CTRL + ALT + T)
	- Digite `sudo apt-get install git`
	- Agora vamos configurar nossas credenciais, ou seja, aquilo que vai dizer que a gente foi que fez tal alteração
	- Digite `git config --global user.name 'seunome'`
	- Digite `git config --global user.email 'seuemail@email.com'`
	- Feito isso, está completa a instalação. 
	
## Como funciona o git ? 
![Descrição](https://blog.cpanel.com/wp-content/uploads/2018/05/image2018-2-8_17-46-1.png)

~~O QUE É ISSO???~~
Isso é basicamente seu código ao longo do tempo de desenvolvimento quando está sendo usado o git.
Você tem um **ramo** principal chamado de ***master*** , sempre que você vai **corrigir erros** ou **adicionar funcionalidades** você cria um novo ramo e só então depois de completo e testado você volta para o ramo principal e **transfere** tudo o que você fez no ramo criado para o ramo principal.

Vamos ver isso na prática!!
 - Crie um arquivo em uma pasta qualquer e chame-o de `index.html`. 
 - Abra esse arquivo em um navegador. 
 - Acesse essa pasta pelo terminal (ou CMD no windows). Caso você tenha o VSCode instalado, abra a pasta usando ele, vai facilitar o processo porque ele tem terminal embutido :).
 - Digite `git init`  no terminal, isso vai dar inicio a um repositório que vai guardar seu projeto.
 - Agora abra o arquivo com um editor de texto e digite algo nele, no meu caso vou colocar `oi esse é um arquivo para aprender sobre git` e uma contagem de 1 até 10 da seguinte forma. 
 
![enter image description here](https://i.imgur.com/SuhfWkb.png)
 - Recarregando a página no navegador você vai poder ver o texto atualizado lá. 
 - Digite `git status` no terminal e você vai ver uma mensagem listando os arquivos e o estado dele em relação ao git, para o `index.html` vai aparecer que não está sendo monitorado. 
 - Agora vamos salvar o que a gente fez, digite `git add index.html` no terminal para adicionar o arquivo ao git e registrar ele pro nosso salvamento. 
 - Agora digite no terminal `git status` e o `index.html` agora vai aparecer como mudanças a serem submetidas.
 - Agora vamos salvar a modificação digita `git commit -m 'meu primeiro commit'` e feito! O comando `commit` indica o salvamento e o  `-m` indica a mensagem que queremos dar para o ele (pra ficar fácil de lembrar dps quando formos ver o histórico de alterações). 
 - Se você der `git status` agora, vai ver que está vazio, pois não há alterações. 

Esse foi o processo básico, quando vocês faz uma alteração são esses os passos a seguir para salvá-las! Agora vamos ver como podemos **trabalhar em conjunto** com outros desenvolvedores usando o **git**.

- Digite `git checkout -b novo_recurso` . Com esse comando você está *pulando pra outro galho que você acabou de criar na sua árvore*!  O galho é um ramo, a árvore é seu repositório. O comando `checkout` serve para mudar de ramo e `-b` indica a criação de um ramo novo, em seguida é o nome do ramo (lembre que antes estávamos no **master**).
- Agora no seu arquivo `index.html`  digite na **linha que tem o número 6**, o seguinte `<button>Novo recurso</button>`. Isso vai criar um botão na página, não vamos entrar em detalhes agora, apenas coloque isso lá. Quando você recarregar a página o botão estará lá no meio do texto.
![enter image description here](https://i.imgur.com/Vpz9naQ.png)
- Feito isso, salve o arquivo e repita o processo de salvamento no git, coloque uma mensagem qualquer (A mensagem é obrigatória). 
- Agora vamos voltar para o ramo principal. Digite `git checkout master`.
- Você vai ver que seu arquivo agora não tem mais aquele botão, mas não se preocupe, vamos pegar esse recurso de volta mais tarde. 
- Vamos criar um novo galho e pular pra ele, digite `git checkout -b modifica`.
- Mude a mensagem do seu `index.html` para `Olá, esse é um arquivo para aprender sobre git!`. 
- Repita o processo de salvar o arquivo no git e volte para o ramo principal. 

Pronto, esse foi o processo de criar novos ramos de inserções de recursos e modificações, porém não acabou, depois disso a gente tem juntar tudo certo? Imagine que quem fez a inserção de um novo recurso foi um amigo seu e que fez a modificação foi uma amiga sua, agora sua tarefa é unir esse ramos na principal, **FUSÃO**! 

- Estando no ramo principal, você vai usar o comando `git merge <branch>` para unir as alterações. 
- Digite `git merge novo_recurso` e `git merge modifica`. 
- Pronto, agora seu arquivo contem as duas alterações, pode ser feliz :)
- *Caso apareça alguma coisa a mais tipo uma mensagem perguntando porque o merge é necessário basta salvar e pronto.*

E se quando eu mostrei pro cliente ele não gostou eu tiver que voltar ao que havia antes? Simples, hora de usar o `git reset`!

- Digite `git log`. Vai aparecer o histórico de commits.
- Você vai se movimentar usando as setas, vá até o que diz 'meu primeiro commit' e copie o código maluco que tem em cima.
![enter image description here](https://i.imgur.com/l913pld.png)
- Esse 65eb1fa47......
- Pra sair é só digitar `:q`
- Agora para voltar tudo é só digitar `git reset --hard seu_codigo`
- Pronto, tudo voltou ao que estava antes.

Agora você entende como o git facilita nossas vidas num trabalho em equipe e a recuperar aquilo que já fizemos anteriormente. 

## Git nas nuvens - Gitlab 
![enter image description here](https://instruct.com.br/wp-content/themes/instruct-beta/img/gitlab.svg)
Hora de conhecer mais um aliado de nossas batalhas!

O Gitlab é uma ferramenta que permite com que você consiga salvar seus repositórios na nuvem e possa acessá-lo de qualquer lugar além de compartilhar ele com seus amiguinhos do trabalho :). 

Ele funciona de forma bem simples, vamos usar aquele nosso projeto antigo pra isso.

 - [Crie sua conta no git lab](https://gitlab.com/users/sign_in) 
 - No seu painel clique em `new project`
![enter image description here](https://i.imgur.com/fRoFxip.png)
 - Preencha as informações do nome e url, depois clique em criar. 
 ![enter image description here](https://i.imgur.com/Vi4xEfQ.png)
- Quando você cria o projeto, ele vai te mandar pra uma página com instruções de como utilizá-lo. Vamos seguir a que fala sobre uma pasta já existente e **pular** a parte do `git init`.
![enter image description here](https://i.imgur.com/rJU4gWH.png)
- O `git remote add origin <url>` diz que você tem um repositório remoto e a origem dele é sua **URL**. 
 - O `git add .` é um comando que adiciona todos seus arquivos de uma só vez no repositório, aí ele está só mostrando um processo de salvamento global antes de enviar pro gitlab, mas nosso projeto está sem modificação, então vamos direto para o `git push -u origin master`. Esse comando envia as modificações para o gitlab.
 - Feito isso, você vai ver que tudo que você fez agora estará lá, e você vai poder acessar de qualquer canto que estiver. 
 - Vamos imaginar que estamos em outro computador e em outra pasta vamos digitar o seguinte comando `git clone URL`, feito isso, todo seu projeto foi baixado e você já pode usá-lo normalmente. 
 - Outros exemplos de uso são:
	 - Meu repositório na máquina está uma versão antiga e no gitlab tem uma nova, para baixar as atualizações basta digitar `git pull origin <branch>`. O mais comum é ser a branch master, mas serve para qualquer um. 
	 - Meu repositório na nuvem está uma versão antiga e na máquina a nova, para enviar as atualizações basta digitar `git push origin <branch>`. O mais comum é ser na branch master.

### Para saber mais!!
- [Todos os comandos do Git](https://gist.github.com/leocomelli/2545add34e4fec21ec16)
- [Tudo que você queria saber sobre Git e GitHub, mas tinha vergonha de perguntar](https://tableless.com.br/tudo-que-voce-queria-saber-sobre-git-e-github-mas-tinha-vergonha-de-perguntar/)
- [git - guia prático](http://rogerdudler.github.io/git-guide/index.pt_BR.html)
