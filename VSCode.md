# Conheça o Visual Studio Code
O Visual Studio Code, o famoso VS Code, é um editor de texto poderoso com muitas funcionalidades imbutidas e muitas extensões para você montar seu ambiente de desenvolvimento da melhor forma possível.

Definimos como um editor de texto, mas ele chega a ser mais do que isso. O VS Code fica em um meio termo entre um editor de texto, como o Sublime e o Atom, e um IDE, como o Visual Studio, por possuir funções dos dois tipos. A melhor definição é que o Visual Studio Code é um editor de código.

É um software gratuito e você pode baixar por meio deste [link](https://code.visualstudio.com/).

![Visual Studio Code](https://i.imgur.com/CDumV6w.jpeg)

### Abrindo meu primeiro projeto...
Para colocar seu projeto dentro do Visual Studio Code basta organizar uma pasta que armazenará os arquivos desse projeto, logo em seguida ir no menu superior em "File" (Arquivo) > "Open Folder" (Abrir pasta).

![](https://i.imgur.com/mAL0iaV.jpeg)

Você pode criar arquivos e pastas dentro do projeto pelo próprio VS Code, basta utilizar esses dois ícones mágicos:

![](https://i.imgur.com/JRG7jbr.jpeg)

### Conhecendo um pouco mais da interface
Temos uma barra lateral, chamada de "Activity Bar":

![](https://i.imgur.com/Wm3Dqwr.jpeg)

Nela você pode encontrar o EXPLORADOR DE ARQUIVOS, que irá auxiliar você na navegação dos arquivos do projeto. Logo embaixo temos o "Search", ele será muito útil, pois possibilita que você localize qualquer termo presente em qualquer arquivo do seu projeto. Em projetos grandes, como aqueles que envolvem os sistemas de Ruby On Rails, isso é MÁGICO!!! O mais sensacional é a possibilidade de especificar que formatos de arquivos serão incluídos ou excluídos. Por exemplo, eu posso encontrar uma determinada função mas quero localizar ela apenas no arquivos da linguagem Ruby.

Complementando essa ferramenta de pesquisa, existe uma forma também de pesquisar pelo nome do arquivo, basta utilizar o comando CTRL + P. 

O "Source Control Management" é a integração com o Git, possibilitando um melhor controle de versão do seu projeto.

Pelo próprio nome da próxima ferramenta sugere temos a reunião de ferramentas de Debugging.

Extenções é outra funcionalidade sensacional do VS Code. Dependendo da linguagem que você for trabalhar, você pode instalar extensões que complementem, auxiliem na sua produtividade e tornem a programação mais prática.

### Command Palette
Com esta funcionalidade, você irá navegar pelas diferentes configurações e menus do VS Code. Para acessar, use o atalho CTRL + SHIFT + P ou vá em "View" (Visualizar) > "Command Palette".  Ao abrir ela, basta digitar o que busca e localizar dentre das opções oferecidas.

Por exemplo, vamos trocar as cores do tema. Pesquise por "color". Provavelmente irá aparecer uma opção das "Preferences" (Preferências) chamada "Color Theme" (Cor do Tema). Ao selecionar essa opção, apareceram várias cores para você colorir a sua programação!!!
 
 ![](https://i.imgur.com/MBUYml3.jpeg)
 
 ## Produtividade na edição
 
## Git intrometido, mas gostamos dele s2
Iremos aprender a como trabalhar com o git no VS Code. Em caso de framework já configurado, tera uma pasta .git que mostra quais arquivos serão ignorados.

Indo na sessão do git. Iremos criar o repositório no VS code, que seria o git init. Depois de criado, clicamos no "+" para adicionar os arquivos no repositório. Cada modificação que for efetuada e você deseja criar um commit, basta usar o campo de texto, colocar a mensagem e dar um CTRL + ENTER para realizar o primeiro commit. 

Podemos associar esse repositório ao GitLab, para isso precisamos conectar o gitlab com o VS Code. No caso, o repositório local saber que temos um repositório remoto. Todo push dado irá mandar as modificações pro repositório remoto.
## Extensões que serão úteis
 ### vscode-icons
 Uma extensão que coloca ícones para identificar arquivos de diferentes extensões. Dessa forma fica mais fácil de diferenciar e localizar os arquivos na hora da programação.

### Rainbow Brackets
Diferencia os parênteses utilizando cores diferentes. Fica bem mais fácil trabalhar, principalmente naquele ponto que você tem (parênteses (dentro (de (parênteses))))

### Indent Rainbow
Assim como a anterior, esta usa as cores para facilitar a localização dentro do código, aqui no caso se refere a indentação.

### Pomodoro Timer
Aos que gostam da técnica pomodoro: desenvolver por 25 min, descansar 5 min, voltar ao desenvolvimento novamente. O relógio fica de uma maneira que não atrapalha o desenvolvimento.

### Bookmarks
Como próprio nome sugere, é uma extensão que possibilita a marcação do código. Por exemplo, se tem algum erro ou detalhe que você precisa ver mais tarde você deixa marcado. A linha de código ficará registrada na aba referente a extensão. 

### Git Lens
Será muito útil para o desenvolvimento de grandes projetos que possua uma equipe. Ele indica quais linhas de códigos foram alteradas, por quem e em qual commit. 

### Faker VS Code
Gera dados e informações fakes para preencher seu código de maneira mais rápido, evitando que você vá para o navegador buscar por estes dados, por exemplo.
 

## Debug


> Written with [StackEdit](https://stackedit.io/).


**VSCode é um editor de texto poderoso com muitas funcionalidades imbutidas e muitas extensões para você montar seu ambiente de desenvolvimento da melhor forma possível.** 

![enter image description here](https://i.imgur.com/jNcy35q.png)
Tem um terminal dentro do editor!!!! :D -1 aba

![](https://i.imgur.com/mf5bA3h.png)
Arquivos do espaço de trabalho. (O seu provavelmente não vai estar igual visualmente porque não tem algumas extensões.)
![enter image description here](https://i.imgur.com/zQVe9WA.png)
Barra de pesquisa com muitas funcionalidades interessantes e de fácil visualização. 

![enter image description here](https://i.imgur.com/WPTDfTM.png)
Git na palma da sua mão, muito fácil de visualizar seus arquivos, diferenciá-los, fazer commits, enviar para o remoto, etc. 

![enter image description here](https://i.imgur.com/szWKLh6.png)
Debug te ajuda a encontrar erros no código, depois vai ter um vídeo especial ensinando a usar isso, pois abrange muita coisa. 

![enter image description here](https://i.imgur.com/IBHYHOm.png)
Essa parte que trás o poder de você moldar a o editor ao seu gosto, a seguir vou listar algumas extensões muito legais!

 - [GitLens](https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens)
- [Indent Rainbow]([https://marketplace.visualstudio.com/items?itemName=oderwat.indent-rainbow](https://marketplace.visualstudio.com/items?itemName=oderwat.indent-rainbow))
- [Material Icon]([https://marketplace.visualstudio.com/items?itemName=PKief.material-icon-theme](https://marketplace.visualstudio.com/items?itemName=PKief.material-icon-theme))
- [Partial Diff](https://marketplace.visualstudio.com/items?itemName=ryu1kn.partial-diff)
- [Rainbow Brackets]([https://marketplace.visualstudio.com/items?itemName=2gua.rainbow-brackets](https://marketplace.visualstudio.com/items?itemName=2gua.rainbow-brackets))
- [Ruby]([https://marketplace.visualstudio.com/items?itemName=rebornix.Ruby](https://marketplace.visualstudio.com/items?itemName=rebornix.Ruby))
- [Ruby on Rails]([https://marketplace.visualstudio.com/items?itemName=hridoy.rails-snippets](https://marketplace.visualstudio.com/items?itemName=hridoy.rails-snippets))